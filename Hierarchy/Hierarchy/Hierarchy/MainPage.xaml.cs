﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hierarchy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        async private void onClickBtn1(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new WhoItIs()); //Push to the ContentPage WhoItIs()
        }
        async private void onClickBtn2(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Biography()); //Push to the ContentPage Biography()
        }
        async private void onClickBtn3(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new RotatePicture()); //Push to the ContentPage RotationPicture()
        }
    }
}

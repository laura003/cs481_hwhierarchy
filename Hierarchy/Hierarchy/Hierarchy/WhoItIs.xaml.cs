﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WhoItIs : ContentPage
    {
        public WhoItIs()
        {
            InitializeComponent();
        }
        async private void onClickBtn1(object sender, EventArgs args)
        {
            await DisplayAlert("Baaaaad", "Nop", "Ask Google"); //Send a popup alert
        }
        async private void onClickBtn2(object sender, EventArgs args)
        {
            await DisplayAlert("Baaaaad", "Try again ...", "Ask Google"); //Send a popup alert
        }
        async private void onClickBtn3(object sender, EventArgs args)
        {
            await DisplayAlert("Yeeaaaaah", "CORRECT !!!!!!!", "Congratulation"); //Send a popup alert
            await Navigation.PushAsync(new WhoItIs2()); //Push to the ContentPage WhoItIs2()
        }
        async private void onClickBtn4(object sender, EventArgs args)
        {
            await DisplayAlert("Baaaaad", "Seriously ..?", "Ask Google"); //Send a popup alert
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Biography : ContentPage
    {
        public Biography()
        {
            InitializeComponent();
        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            DisplayAlert("Hello", "This is a biography of Louis De Funes", "Ok"); //Send a message  when this page appear
        }
    }
}
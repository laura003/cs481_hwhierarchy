﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WhoItIs2 : ContentPage
    {
        public WhoItIs2()
        {
            InitializeComponent();
        }
        async private void onClickBtn1(object sender, EventArgs args)
        {
            await DisplayAlert("Baaaaad", "Nop", "Ask Google"); //Send a popup alert
        }
        async private void onClickBtn2(object sender, EventArgs args)
        {
            await DisplayAlert("Yeeaaaaah", "CORRECT !!!!!!!", "Congratulation"); //Send a popup alert
        }
        async private void onClickBtn3(object sender, EventArgs args)
        {
            await DisplayAlert("Baaaaad", "Try again ...", "Ask Google"); //Send a popup alert
        }
        async private void onClickBtn4(object sender, EventArgs args)
        {
            await DisplayAlert("Baaaaad", "Seriously ..?", "Ask Google"); //Send a popup alert
        }
        async private void onClickBtn5(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync(); //supp all stack of ContentPage
        }

        async private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            await DisplayAlert("Menu", "Here is the menu", "Nice"); //Send a message  when this page disappear
        }
    }
}